---
layout: handbook-page-toc
title: "Customer Segments & Associated Metrics"
---

## Overview

The following outlines the customer segments supported by the Technical Account Management organization and the metrics for success for each segment in addition to our top-level metrics of net revenue retention (NRR) growth and gross retention.

The segments shown are currently in development, and are not yet implemented. For our current account alignment model, please see our [Account Engagement](/handbook/customer-success/tam/engagement/) information.
{: .alert .alert-info}

## High-Touch: Enterprise

Definition: Named TAM on the account, enabling long-term strategic goals and 1:1 TAM to customer enablement/expansion strategies. 

#### Align

[Success Plans](handbook/customer-success/tam/success-plans/): The success plan ensures that we strategically engage around customer platform value and not features & functions. This focus ensures that we are spending time on ensuring customer success and growth while defending customer retention.

[EBRs](/handbook/customer-success/tam/ebr/):  These strategic touchpoints ensure that progress against desired business outcomes is being made and communicated back to the team that we work with and key influencers/decision-makers. It also creates a vehicle for introducing senior leadership into the customer account, enabling more strategic touchpoints.

Metrics:

1. Management-qualified Success Plans in all high-touch accounts
1. EBRs completed within the last twelve months in 75% of all accounts


#### Enable

The primary objective in enabling our customers is to get a customer to value quickly, ensure any roadblocks to adoption are removed and get the customer to a state of maturity in their desired use cases to provide platform value early on.

Metrics:

1. [Time to first engage](/handbook/customer-success/tam/onboarding/#time-to-engage) (14 days): Requires a smooth transition from pre to post-sales, ensuring that the TAM is set up to quickly move the customer from pre-sales POV state to post-sales full instance established
1. [Time to 1st Value](/handbook/customer-success/tam/onboarding/#time-to-first-value) (30 days) Indicates challenges in the account if not met in the initial roll-out; speaks to the success of the TAM in ensuring initial adoption roadblocks are removed
1. [Time to Onboard](/handbook/customer-success/tam/onboarding/#time-to-onboard) (45 days) Indicates when a customer is enablement-ready and prepared to move to a more consistent cadence of check-ins as the customer continues to roll out their build.  This metric also speaks to the efficacy of the TAM engagement in the early weeks
1. Stage Enablement Playbooks Completed (QoQ)
1. FY23 Metric: Use Case/Stage Adoption Maturity Score: As we begin to have product analytics, we can score the platform adoption per use cases against our best practice benchmarks for mature product adoption, and proactively address areas for improvement.  In FY23 we will prioritize the maturity scores for CI and for DevSecOps, in-line with our Big Rock of ['Expertise in driving CI and DevSecOps adoption & expansion'](https://about.gitlab.com/handbook/customer-success/tam/#big-rock-2-expertise-in-driving-ci-and-devsecops-adoption--expansion) This today is done through cadence calls and qualitative conversations in the absence of maturity analytics.

#### Expand & Renew

This phase is about going beyond a customer’s existing use cases, into additional adoption/expansion and tier upgrades.  In ensuring a customer gets to value quickly, in understanding a customer’s desired business outcomes, and in engaging strategically through touchpoints such as EBRs in addition to cadence calls, a TAM is ‘given permission’ as a trusted advisor to introduce and advocate for the idea of growth.

Metrics:

1. Stage Expansion Playbooks Completed (QoQ)
1. Days per Playbook Completed per Stage (QoQ)
1. Win rate for expansion playbooks (QoQ)



## Mid-Touch

Definition: Named TAM on the account, product usage data-based success goals per account, programmatic enablement.

#### Align 

Here Success Plans are more focused on quantitative objectives such as use case adoption, tied into value drivers as determined in the pre-sales process, to ensure platform value is realized. Progress and success against these objectives are reported upon in the EBRs that will be held with those customers that have product usage data available.

Metrics:

1. 100% net-new customers with success plans (value drivers, use cases)
1. EBRs held with 50% of customers with product usage data 

#### Enable

Onboarding and enablement for this cohort is primarily through webinar cohorts, with TAM touchpoints throughout the first 60 days to ensure the customer is setup for success and has overcome initial roadblocks to adoption and value.  Enablement webinars and adhoc sessions with the TAM ensure the customer has key adoption questions answered and has the best practice guidance needed to be successful.  Use Case Health Scores enable the TAM to track the efficacy of these programs and determine strategic reach-outs.

Metrics: 

Onboarding:
1. Time to first engage: 14 days
1. Time to 1st Value: 30 days 
1. Time to Onboard: 45 days 
1. Net-new customer attendance in onboarding webinars
1. Onboarding NPS & CSAT scores


Use-Case Enablement
1. Customer attendance in enablement webinars
1. Use Case Health scores

#### Expand & Renew

Expansion is primarily driven by the SAL or AE in this segment, though a key driver for expansion is product enablement and familiarity.  Webinars are the primary means of driving interest for customers into new segments, the success of which is tracked through customer engagement scorecards and product usage data insights around new use cases adopted.  Low-license utilization reports will focus the TAM on identifying those customers at risk of contraction. The renewal NPS/CSAT survey 110 days before renewal enables the TAM to identify customers that may be challenged at the point of renewal and are not easily identifiable as challenged through product usage data. 

1. Customer attendance in expansion webinars
1. New Use Cases Adopted
1. % Low License Utilizations 'Saves' (improvement)
1. Renewal NPS & CSAT Scores


## Scale 

Definition: Pooled TAMs driving programmatic enablement and some customer engagement.

#### Align 

This cohort is primarily enabled through content and webinars, and the success plan will be driven in year 1 of a customer's lifecycle by the desired value drivers from the command plan, and will be used for strategic engagements in customers outside of onboarding. 


#### Enable

Onboarding and enablement for this cohort is primarily through webinar cohorts, with a TAM reach-out at day 30 to ensure the customer is on-track for success.  There are additional reach-outs for triggered events such as missed time-to-first-value and poor onboarding NPS scores. 

Onboarding:
1. Net-new customer attendance in onboarding webinars
1. Time to First Value
1. % 30 day calls completed
1. Onboarding NPS & CSAT scores

Use-Case Enablement:
1. Customer attendance in enablement webinars
1. Use Case Health scores


#### Expand & Renew

Expansion is primarily driven by the SAL or AE in this segment, though a key driver for expansion is product enablement and familiarity. 

1. Customer attendance in expansion webinars
1. New Use Cases Adopted
1. Renewal NPS & CSAT Scores


## Digital-Touch

#### Align

Success for this cohort is primarily reported upon through product usage data insights, and through NPS/CSAT survey results 

#### Enable

We will seek to remove adoption roadblocks and poor setup scenarios (frequent issue) and enable use-case/stage adoption maturity through admin enablement via email.  In FY22 we will augment this email program with other vehicles such as downloadable issue boards, guided video paths, guided blog series, and Slack Bots.

1. Metric: Open Rate
1. Metric: Click-Through Rate
1. Metric: [Time to 1st Value](/handbook/customer-success/tam/onboarding/#time-to-first-value)
1. Metric: End of Onboarding NPS & CSAT results

### Expand

We have legal permission to email users for enablement only, so will seek to drive expansion through suggested next-steps via the FY22 vehicles such as issue boards with task lists, while continuing to partner with other teams such as the Marketing and Growth teams on use case adoption and maturity with key next steps into new stages via key points such as introducing SAST/DAST scanning (leading to Secure, Ultimate).

1. Metric: Faster time to IACV growth against FY21 cohort
