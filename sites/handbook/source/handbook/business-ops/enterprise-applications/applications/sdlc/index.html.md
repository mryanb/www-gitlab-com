---
layout: handbook-page-toc
title: "Order-to-Cash SDLC Process"
description: "The Enterprise Applications Team implements and supports specialized applications that support our business processes within GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Order-to-Cash Applications
The Enterprise Applications team helps to support the full order to cash business process. As part of this process, there are a number of applications GitLab uses, including:
1. [Zuora Billing](https://about.gitlab.com/handbook/business-ops/enterprise-applications/guides/zuora/#zuora)
1. [Zuora Revenue](https://about.gitlab.com/handbook/business-ops/enterprise-applications/guides/zuora/#revenue)
1. CustomersDot
3. LicenseDot
4. Salesforce
5. Stripe
6. Avalara
7. Netsuite
8. Workato


## SDLC Process Stacks
In order to ensure an efficient and effective development lifecycle process for these applications, the Enterprise Applications team has set up three unique "stacks" aligned to the different SDLC process steps:
1. **Development / QA** - stack of sandboxes dedicated to development work and internal IT testing
   * Consists of isolated sandboxes to allow for development work without any interference
2. **Staging / UAT** - stack of sandboxes mimicking a production environment for business team testing
   * These are generally full data copy sandboxes refreshed frequently and useful for accurate acceptance testing in a production-like environment
3. **Production** - stack of the single live, real environment for each application


## Sandbox Environments
In accordance with these three process steps, sandbox environments have been grouped and integrated accordingly. The current sandbox stacks are listed in the table below.

| Application         | Development / QA                   | Staging / UAT                     |
|---------------------|------------------------------------|-----------------------------------|
| **SFDC**            | Sandbox (00D0m000000Deo9)          | Staging (00D63000000IO3F)         |
| **Zuora Billing**   | Zuora Billing API Sandbox  (14115) | Zuora Central Sandbox (10000719)  | 
| **Zuora Revenue**   | REVPRO_GITLAB_S01                  | REVPRO_GITLAB_S02                 |
| **NetSuite**        | Sandbox 1                          | Sandbox 2                         |
| **Workato**         | Development Sandbox                | Testing Sandbox                   |
| **Stripe**          | about.gitlab test                  | about.gitlab test                 |
| **Avalara**         | Avalara Sandbox<sup>1</sup>        | Avalara Sandbox<sup>1</sup>                   |
| **CustomerDot**     | Staging Environment                | Staging Environment               |
| **LicenseDot**      | Staging Environment                | Staging Environment               |

<sup>1</sup> _Currently pending acquisition._


## Sandbox Integrations
Among each step in the SDLC process, integrations between sandboxes have been set up to allow for cross-application development and testing.

<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/a73af76f-998e-4b5c-ae5d-3e35fcb17429" id="ou2mt9KZlgjq"></iframe></div>
